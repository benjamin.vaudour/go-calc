package calc

import (
	"fmt"
	"strings"

	"framagit.org/benjamin.vaudour/strutil/align"
)

type Register map[string]Stack

func (r Register) Exists(key string) bool {
	_, exists := r[key]
	return exists
}
func (r Register) Set(key string, numbers ...*Number) { r[key] = Stack(numbers).Clone() }
func (r Register) Get(key string) (s Stack, err error) {
	var exists bool
	if s, exists = r[key]; exists {
		s = s.Clone()
	} else {
		err = ErrNotExist(reg(key))
	}
	return
}

func (r Register) Clear(key string) (err error) {
	if r.Exists(key) {
		delete(r, key)
	} else {
		err = ErrNotExist(reg(key))
	}
	return
}
func (r Register) ClearAll() (err error) {
	if len(r) == 0 {
		err = ErrIsEmpty(errMemory)
	} else {
		for k := range r {
			delete(r, k)
		}
	}
	return
}

func (r Register) Format(f ...FormatFunc) (str string, err error) {
	if len(r) == 0 {
		err = ErrIsEmpty(errMemory)
	} else {
		a := 2
		for k := range r {
			if l := len(k) + 2; l > a {
				a = l
			}
		}
		var lines []string
		for k, s := range r {
			l, _ := s.FormatAll(f...)
			k = align.Left(fmt.Sprintf("'%s'", k), a)
			lines = append(lines, fmt.Sprintf("%s → %s", k, l))
		}
		str = strings.Join(lines, "\n")
	}
	return
}
