package calc

import (
	"fmt"
	"strings"
)

type Stack []*Number

func (s Stack) Clone() Stack {
	out := make(Stack, len(s))
	for i, n := range s {
		out[i] = n.Clone()
	}
	return out
}
func (s Stack) Len() int { return len(s) }

func (s Stack) GetLast() (n *Number, err error) {
	if i := s.Len() - 1; i >= 0 {
		return s[i], nil
	}
	return nil, ErrIsEmpty(errStack)
}
func (s Stack) GetLast2() (n1, n2 *Number, err error) {
	if i := s.Len() - 2; i >= 0 {
		return s[i], s[i+1], nil
	}
	return nil, nil, ErrLenLt2(errStack)
}

func (s *Stack) Pop() (n *Number, err error) {
	if n, err = s.GetLast(); err == nil {
		*s = (*s)[:s.Len()-1]
	}
	return
}
func (s *Stack) Pop2() (n1, n2 *Number, err error) {
	if n1, n2, err = s.GetLast2(); err == nil {
		*s = (*s)[:s.Len()-2]
	}
	return
}

func (s *Stack) Clear() (err error) {
	_, err = s.Pop()
	return
}
func (s *Stack) ClearAll() (err error) {
	if s.Len() == 0 {
		err = ErrIsEmpty(errStack)
	} else {
		*s = (*s)[:0]
	}
	return
}

func (s *Stack) Add(numbers ...*Number) {
	c := Stack(numbers).Clone()
	*s = append(*s, c...)
}
func (s *Stack) Duplicate() (err error) {
	var n *Number
	if n, err = s.GetLast(); err == nil {
		s.Add(n)
	}
	return
}
func (s *Stack) DuplicateAll() (err error) {
	if s.Len() > 0 {
		s.Add((*s)...)
	} else {
		err = ErrIsEmpty(errStack)
	}
	return
}

func (s *Stack) Swap(i, j int) { (*s)[i], (*s)[j] = (*s)[j], (*s)[i] }
func (s *Stack) Reverse() (err error) {
	if i := s.Len() - 2; i >= 0 {
		s.Swap(i, i+1)
	} else {
		err = ErrLenLt2(errStack)
	}
	return
}
func (s *Stack) ReverseAll() (err error) {
	if l := s.Len(); l >= 2 {
		l2 := l >> 1
		for i := 0; i < l2; i++ {
			s.Swap(i, l-1-i)
		}
	} else {
		err = ErrLenLt2(errStack)
	}
	return
}

func (s *Stack) Op1(f Op1Func) error {
	n, err := s.Pop()
	if err == nil {
		s.Add(f(n))
	}
	return err
}
func (s *Stack) Op2(f Op2Func) error {
	n1, n2, err := s.Pop2()
	if err == nil {
		s.Add(f(n1, n2))
	}
	return err
}

func (s Stack) Format(f ...FormatFunc) (str string, err error) {
	var cb FormatFunc
	if len(f) > 0 && f[0] != nil {
		cb = f[0]
	} else {
		cb = Config.Number
	}
	var n *Number
	if n, err = s.GetLast(); err == nil {
		str = cb(n)
	}
	return
}
func (s Stack) FormatAll(f ...FormatFunc) (str string, err error) {
	var cb FormatFunc
	if len(f) > 0 && f[0] != nil {
		cb = f[0]
	} else {
		cb = Config.Number
	}
	l := s.Len()
	snb := make([]string, l)
	for i, n := range s {
		snb[i] = cb(n)
	}
	if l == 0 {
		err = ErrIsEmpty(errStack)
	}
	str = fmt.Sprintf("( %s )", strings.Join(snb, " "))
	return
}
