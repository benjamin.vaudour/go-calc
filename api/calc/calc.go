package calc

import (
	"fmt"
	"io"
	"os"
)

type ExecuteFunc func(*Calc, ...string) error
type ArgParserFunc func(string) ([]string, error)

type Calc struct {
	Stack    Stack
	Register Register
	Macro    Macro
	Callback ExecuteFunc
	Parser   ArgParserFunc
	Stdout   io.Writer
	Stderr   io.Writer
	Config   FormatConfig
}

func New() *Calc {
	return &Calc{
		Stdout:   os.Stdout,
		Stderr:   os.Stderr,
		Register: make(Register),
		Macro:    make(Macro),
		Config:   Config,
	}
}

func (c *Calc) SetWarningStyles(styles ...string) { c.Config.WarningStyle = styles }
func (c *Calc) SetErrorStyles(styles ...string)   { c.Config.ErrorStyle = styles }
func (c *Calc) SetOptions(display DisplayType, precision int, auto bool) {
	c.Config.DisplayType = display
	c.Config.Precision = precision
	c.Config.PrecisionAuto = auto
}

func (c *Calc) Println(s string)    { fmt.Fprintln(c.Stdout, s) }
func (c *Calc) Warningln(err error) { fmt.Fprintln(c.Stderr, c.Config.Warning(err)) }
func (c *Calc) Errorln(err error)   { fmt.Fprintln(c.Stderr, c.Config.Error(err)) }
func (c *Calc) PrintOrErr(cb func() (string, error)) {
	if s, err := cb(); err == nil {
		c.Println(s)
	} else {
		c.Errorln(err)
	}
}
func (c *Calc) PrintOrWarn(cb func() (string, error)) {
	if s, err := cb(); err == nil {
		c.Println(s)
	} else {
		c.Warningln(err)
	}
}

func (c *Calc) printWarn(f func(...FormatFunc) (string, error), ff ...FormatFunc) {
	cb := c.Config.Number
	if len(ff) > 0 && ff[0] != nil {
		cb = ff[0]
	}
	c.PrintOrWarn(func() (string, error) { return f(cb) })
}
func (c *Calc) printErr(f func(...FormatFunc) (string, error)) {
	c.PrintOrErr(func() (string, error) { return f(c.Config.Number) })
}

func (c *Calc) PrintOptions()                  { c.Println(c.Config.String()) }
func (c *Calc) PrintStack(f ...FormatFunc)     { c.printWarn(c.Stack.FormatAll, f...) }
func (c *Calc) PrintLast(f ...FormatFunc)      { c.printWarn(c.Stack.Format, f...) }
func (c *Calc) PrintRegisters(f ...FormatFunc) { c.printWarn(c.Register.Format, f...) }
func (c *Calc) PrintMacros()                   { c.Println(c.Macro.Format()) }

func (c *Calc) ClearLast() {
	if err := c.Stack.Clear(); err != nil {
		c.Warningln(err)
	}
}
func (c *Calc) ReverseLast() {
	if err := c.Stack.Reverse(); err != nil {
		c.Warningln(err)
	}
}
func (c *Calc) DuplicateLast() {
	if err := c.Stack.Duplicate(); err != nil {
		c.Warningln(err)
	}
}

func (c *Calc) ClearStack() {
	if err := c.Stack.ClearAll(); err != nil {
		c.Warningln(err)
	}
}
func (c *Calc) ReverseStack() {
	if err := c.Stack.ReverseAll(); err != nil {
		c.Warningln(err)
	}
}
func (c *Calc) DuplicateStack() {
	if err := c.Stack.DuplicateAll(); err != nil {
		c.Warningln(err)
	}
}

func (c *Calc) AddStack(numbers ...*Number) { c.Stack.Add(numbers...) }
func (c *Calc) AddLenStack()                { c.AddStack(Int(c.Stack.Len())) }

func (c *Calc) Op1(f Op1Func) {
	if err := c.Stack.Op1(f); err != nil {
		c.Errorln(err)
		c.Stack.ClearAll()
	}
}
func (c *Calc) Op2(f Op2Func) {
	if err := c.Stack.Op2(f); err != nil {
		c.Errorln(err)
		c.Stack.ClearAll()
	}
}

func (c *Calc) Load(key string) {
	if stk, err := c.Register.Get(key); err == nil {
		c.AddStack(stk...)
	} else {
		c.Warningln(err)
	}
}
func (c *Calc) StoreLast(key string) {
	if nb, err := c.Stack.Pop(); err == nil {
		c.Register.Set(key, nb)
	} else {
		c.Warningln(err)
	}
}
func (c *Calc) StoreStack(key string) {
	stk := c.Stack.Clone()
	if err := c.Stack.ClearAll(); err == nil {
		c.Register.Set(key, stk...)
	} else {
		c.Warningln(err)
	}
}

func (c *Calc) ExistRegister(key string) bool { return c.Register.Exists(key) }
func (c *Calc) ClearRegister(key string) {
	if err := c.Register.Clear(key); err != nil {
		c.Warningln(err)
	}
}
func (c *Calc) ClearRegisters() {
	if err := c.Register.ClearAll(); err != nil {
		c.Warningln(err)
	}
}

func (c *Calc) Execute(args ...string) {
	if c.Callback == nil {
		panic(errCallback)
	}
	if err := c.Callback(c, args...); err != nil {
		c.Errorln(err)
		c.Stack.ClearAll()
	}
}

func (c *Calc) ExistMacro(key string) bool { return c.Macro.Exists(key) }
func (c *Calc) ClearMacro(key string) {
	if err := c.Macro.Clear(key); err != nil {
		c.Warningln(err)
	}
}
func (c *Calc) ClearMacros() {
	if err := c.Macro.ClearAll(); err != nil {
		c.Warningln(err)
	}
}

func (c *Calc) SetMacro(key, arg string) { c.Macro[key] = arg }
func (c *Calc) ExecMacro(key string) {
	if c.Parser == nil {
		panic(errParser)
	}
	arg, err := c.Macro.Get(key)
	if err == nil {
		var args []string
		args, err = c.Parser(arg)
		if err == nil {
			c.Execute(args...)
			return
		}
	}
	c.Errorln(err)
	c.Stack.ClearAll()
}
