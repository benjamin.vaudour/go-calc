package calc

import (
	"fmt"

	"framagit.org/benjamin.vaudour/number"
	"framagit.org/benjamin.vaudour/strutil/style"
)

type DisplayType int

const (
	DisplayAuto DisplayType = iota
	DisplayDecimal
	DisplayRational
	DisplayScientific
)

type FormatFunc func(*Number) string

type FormatConfig struct {
	DisplayType
	PrecisionAuto bool
	Precision     int
	WarningStyle  []string
	ErrorStyle    []string
}

func (c FormatConfig) Error(err error) string {
	return style.Format(err.Error(), c.ErrorStyle...)
}

func (c FormatConfig) Warning(err error) string {
	return style.Format(err.Error(), c.WarningStyle...)
}

func (c FormatConfig) Number(n *Number) string {
	switch c.DisplayType {
	case DisplayDecimal:
		return number.DecimalString(n, c.Precision, c.PrecisionAuto, n.GetBase())
	case DisplayRational:
		return number.RationalString(n, n.GetBase())
	case DisplayScientific:
		return number.ScientificStringOf(n, c.Precision, c.PrecisionAuto, n.GetBase())
	}
	return number.StringOf(n, c.Precision, c.PrecisionAuto, n.GetBase())
}

func (c FormatConfig) String() string {
	display := "normal"
	switch c.DisplayType {
	case DisplayDecimal:
		display = "décimal"
	case DisplayRational:
		display = "rationnel"
	case DisplayScientific:
		display = "scientifique"
	}
	auto := "fixe"
	if c.PrecisionAuto {
		auto = "automatique"
	}
	return fmt.Sprintf(
		`type d’affichage : %s
précision :        %d (%s)`,
		display,
		c.Precision,
		auto,
	)
}

var Config = FormatConfig{
	DisplayType:   DisplayAuto,
	PrecisionAuto: true,
	Precision:     10,
	WarningStyle:  []string{"yellow"},
	ErrorStyle:    []string{"red"},
}
