package calc

import (
	"fmt"
	"strings"

	"framagit.org/benjamin.vaudour/strutil/align"
)

type Macro map[string]string

func (m Macro) Exists(key string) bool {
	_, exists := m[key]
	return exists
}
func (m Macro) Get(key string) (args string, err error) {
	if s, exists := m[key]; exists {
		args = s
	} else {
		err = ErrNotExist(mac(key))
	}
	return
}

func (m Macro) Clear(key string) (err error) {
	if m.Exists(key) {
		delete(m, key)
	} else {
		err = ErrNotExist(mac(key))
	}
	return
}
func (m Macro) ClearAll() (err error) {
	if len(m) == 0 {
		err = ErrIsEmpty(errMacro)
	} else {
		for k := range m {
			delete(m, k)
		}
	}
	return
}

func (m Macro) Format() (str string) {
	if len(m) > 0 {
		a := 2
		for k := range m {
			if l := len(k) + 2; l > a {
				a = l
			}
		}
		var lines []string
		for k, s := range m {
			k = align.Left(fmt.Sprintf("'%s'", k), a)
			lines = append(lines, fmt.Sprintf("%s → [%s]", k, s))
		}
		str = strings.Join(lines, "\n")
	}
	return
}
