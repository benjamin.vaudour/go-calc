package calc

import (
	"framagit.org/benjamin.vaudour/number"
)

type Number struct {
	number.Number
	b int
}

func (n *Number) Clone() *Number {
	return &Number{
		Number: number.Clone(n),
		b:      n.b,
	}
}

func (n *Number) GetBase() int {
	return n.b
}

func (n *Number) SetBase(base int) (ok bool) {
	if ok = base > 1 && base <= 63; ok {
		n.b = base
	}
	return
}

func Int(n int) *Number {
	return &Number{
		Number: number.NewInt(int64(n)),
		b:      10,
	}
}

type Op1Func func(*Number) *Number
type Op2Func func(*Number, *Number) *Number

func Exec1(f number.Op1Func, n *Number) *Number {
	return &Number{
		Number: f(n),
		b:      n.b,
	}
}

func Exec2(f number.Op2Func, n1, n2 *Number) *Number {
	return &Number{
		Number: f(n1, n2),
		b:      n1.b,
	}
}

func Conv1(f number.Op1Func) Op1Func {
	return func(n *Number) *Number { return Exec1(f, n) }
}

func Conv2(f number.Op2Func) Op2Func {
	return func(n1, n2 *Number) *Number { return Exec2(f, n1, n2) }
}
