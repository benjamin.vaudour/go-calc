package cli

import (
	"os"

	"framagit.org/benjamin.vaudour/go-calc/api/calc"
	"framagit.org/benjamin.vaudour/number"
)

func conv(base int) calc.Op1Func {
	return func(n *calc.Number) *calc.Number {
		n.SetBase(base)
		return n
	}
}

var b2op2 = func(f func(number.Number, number.Number) bool) calc.Op2Func {
	return func(n1, n2 *calc.Number) *calc.Number {
		if f(n1, n2) {
			return calc.Int(1)
		}
		return calc.Int(0)
	}
}

var (
	op1 = map[string]func(*calc.Calc) calc.Op1Func{
		"|": func(*calc.Calc) calc.Op1Func { return calc.Conv1(number.Abs) },
		"v": func(c *calc.Calc) calc.Op1Func {
			precision := c.Config.Precision
			return func(n *calc.Number) *calc.Number {
				base := n.GetBase()
				result := &calc.Number{
					Number: number.Sqrt(n, precision, base),
				}
				result.SetBase(base)
				return result
			}
		},
		"!":  func(*calc.Calc) calc.Op1Func { return calc.Conv1(number.Fact) },
		"++": func(*calc.Calc) calc.Op1Func { return calc.Conv1(number.Inc) },
		"--": func(*calc.Calc) calc.Op1Func { return calc.Conv1(number.Dec) },
		"ci": func(*calc.Calc) calc.Op1Func {
			return func(n *calc.Number) *calc.Number {
				result := &calc.Number{
					Number: number.NewInteger(number.Int(n)),
				}
				result.SetBase(n.GetBase())
				return result
			}
		},
		"cd": func(*calc.Calc) calc.Op1Func { return conv(10) },
		"cb": func(*calc.Calc) calc.Op1Func { return conv(2) },
		"co": func(*calc.Calc) calc.Op1Func { return conv(8) },
		"cx": func(*calc.Calc) calc.Op1Func { return conv(16) },
	}

	op2 = map[string]func(*calc.Calc) calc.Op2Func{
		"+":  func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.Add) },
		"-":  func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.Sub) },
		"*":  func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.Mul) },
		"/":  func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.Div) },
		"//": func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.Quo) },
		"÷":  func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.Quo) },
		"%":  func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.Rem) },
		">>": func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.RShift) },
		"<<": func(*calc.Calc) calc.Op2Func { return calc.Conv2(number.LShift) },
		"^": func(c *calc.Calc) calc.Op2Func {
			precision := c.Config.Precision
			return func(n1, n2 *calc.Number) *calc.Number {
				base := n1.GetBase()
				result := &calc.Number{
					Number: number.Pow(n1, n2, precision, base),
				}
				result.SetBase(base)
				return result
			}
		},
		"<=>": func(*calc.Calc) calc.Op2Func {
			return func(n1, n2 *calc.Number) *calc.Number { return calc.Int(number.Cmp(n1, n2)) }
		},
		"=":  func(*calc.Calc) calc.Op2Func { return b2op2(number.Eq) },
		"<>": func(*calc.Calc) calc.Op2Func { return b2op2(number.Ne) },
		"≠":  func(*calc.Calc) calc.Op2Func { return b2op2(number.Ne) },
		">":  func(*calc.Calc) calc.Op2Func { return b2op2(number.Gt) },
		"<":  func(*calc.Calc) calc.Op2Func { return b2op2(number.Lt) },
		">=": func(*calc.Calc) calc.Op2Func { return b2op2(number.Ge) },
		"≥":  func(*calc.Calc) calc.Op2Func { return b2op2(number.Ge) },
		"<=": func(*calc.Calc) calc.Op2Func { return b2op2(number.Le) },
		"≤":  func(*calc.Calc) calc.Op2Func { return b2op2(number.Le) },
	}

	opc = map[string]func(*calc.Calc){
		"l":  func(c *calc.Calc) { c.AddLenStack() },
		"n":  func(c *calc.Calc) {},
		"h":  func(c *calc.Calc) { c.Println(help) },
		"q":  func(c *calc.Calc) { os.Exit(0) },
		"O":  func(c *calc.Calc) { c.PrintOptions() },
		"p":  func(c *calc.Calc) { c.PrintLast() },
		"P":  func(c *calc.Calc) { c.PrintStack() },
		"c":  func(c *calc.Calc) { c.ClearLast() },
		"C":  func(c *calc.Calc) { c.ClearStack() },
		"d":  func(c *calc.Calc) { c.DuplicateLast() },
		"D":  func(c *calc.Calc) { c.DuplicateStack() },
		"r":  func(c *calc.Calc) { c.ReverseLast() },
		"R":  func(c *calc.Calc) { c.ReverseStack() },
		":P": func(c *calc.Calc) { c.PrintRegisters() },
		":C": func(c *calc.Calc) { c.ClearRegisters() },
		";P": func(c *calc.Calc) { c.PrintMacros() },
		";C": func(c *calc.Calc) { c.ClearMacros() },
	}
)
