package cli

const help = `
Affichage de l’aide
  h                             Affiche l’aide

Format des nombres
  [+|-]{e}                      Nombre entier
  [+|-]{e}.{d}                  Nombre décimal
  <decimal|entier>e<entier>     Nombre décimal en notation scientifique (base 10)
  <decimal|entier en base>×{b}^[+|-]{e}
                                Nombre décimal en notation scientifique (base quelconque)
  <entier>/<entier>             Nombre rationnel
  0%{n}                         Nombre en base 2
  1%{n}                         Nombre en base 2 négatif
  0o{n}                         Nombre en base 8
  1o{n}                         Nombre en base 8 négatif
  0x{n}                         Nombre en base 16
  1x[{n}                        Nombre en base 16 négatif
  ({b})[+|-]{n}                 Nombre en base b

Options d’impression
  O                             Imprime les options d’affichage
  on{n}                         Affichage des nombres au format par défaut, à précision automatique
  oN{n}                         Affichage des nombres au format par défaut, à précision fixe
	od{n}                         Affichage des nombres sous forme décimale à précision automatique
	oD{n}                         Affichage des nombres sous forme décimale à précision fixe
	os{n}                         Affichage des nombres sous forme scientifique à précision automatique
	oS{n}                         Affichage des nombres sous forme scientifique à précision fixe
	or{n}                         Affichage des nombres sous forme rationnellle, calcul à précision automatique
	oR{n}                         Affichage des nombres sous forme rationnellle, calcul  à précision fixe

Gestion de la pile
  p                             Impression du dernier élément de la pile
  P                             Impression de toute la pile
  p{f}{n}                       Impression du dernier élément de la pile suivant le format défini (voir les options d’impression)
  P{f}{n}                       Impression de toute la pile suivant le format défini (voir les options d’impression)
  c                             Suppression du dernier élément
  C                             Réinitialisation de toute la pile
  d                             Duplication du dernier élément
  D                             Duplication de toute la pile
  r                             Inversion de l’ordre des deux derniers éléments
  R                             Inversion de l’ordre de tous les éléments de la pile

Gestion des registres
  :P                            Impression des registres
  :P{f}{n}                      Impression des registres suivant le format défini (voir les options d’impression)
  :C                            Efface tous les registres
  :L{r}                         Charge le registre r dans la pile
  :s{r}                         Stocke le dernier élément de la pile dans le registre r et le supprime
  :S{r}                         Stocke le contenu de la pile dans le registre r et la réinitialise
  :c{r}                         Efface le registre r

Gestion des macros
  ;P                            Impression des macros
  ;C                            Efface toutes les macros
  [{str}]{m}                    Stocke la chaîne en tant que macro dans m
  ;x{m}                         Exécute la macro m
  ;c{m}                         Efface la macro

Arithmétique sur 1 élément
  |                             Valeur absolue
  v                             Racine carrée
  !                             Factorielle
  ci                            Conversion en entier
  cd                            Conversion en base décimale
  cb                            Conversion en base 2
  co                            Conversion en base 8
  cx                            Conversion en base 16
  c{n}                          Conversion en base n
  ++                            Ajoute 1
  --                            Soustrait 1

Arithmétique sur 2 éléments
  +                             Addition
  -                             Soustraction
  *, ×                          Multiplication
  /                             Division
  //, ÷                         Division entière
  %                             Modulo
  ^                             puissance
  >>                            Déplace les bits vers la droite
  <<                            Déplace les bits vers la gauche
  <=>                           Comparaison
  =                             Égalité
  <>, ≠                         Différence
  >                             Supériorité stricte
  <                             Infériorité stricte
  >=, ≥                         Supériorité
  <=, ≤                         Infériorité

Actions spéciales
  l                             Ajoute la taille de la pile dans la pile
  ?                             Dépile le dernier élément de la pile pour sélectionner le prochain argument à utiliser
                                - Si la pile est vide ou que l’élément dépilé vaut 0 ou <NaN>, saute le prochain argument
                                - Sinon, saute le deuxième prochain argument
  n                             Ne fait aucune action
`
