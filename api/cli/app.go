package cli

import (
	"bufio"
	"errors"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"

	"framagit.org/benjamin.vaudour/go-calc/api/calc"
	"framagit.org/benjamin.vaudour/number"
	"framagit.org/benjamin.vaudour/shell/console"
)

func execopt(c *calc.Calc, arg string) error {
	if len(arg) < 3 {
		return calc.ErrIsUnknow(arg)
	}
	precision, err := strconv.Atoi(arg[2:])
	if err != nil {
		return calc.ErrIsUnknow(arg)
	}
	var dt calc.DisplayType
	dtParsed := true
	switch arg[1] {
	case 'n', 'N':
		dt = calc.DisplayAuto
	case 'd', 'D':
		dt = calc.DisplayDecimal
	case 's', 'S':
		dt = calc.DisplayScientific
	case 'r', 'R':
		dt = calc.DisplayRational
	default:
		dtParsed = false
	}
	if !dtParsed {
		return calc.ErrIsUnknow(arg)
	}
	auto := arg[1] >= 'a' && arg[1] <= 'z'
	c.SetOptions(dt, precision, auto)
	return nil
}
func execprint(c *calc.Calc, arg string) error {
	if len(arg) < 3 {
		return calc.ErrIsUnknow(arg)
	}
	precision, err := strconv.Atoi(arg[2:])
	if err != nil {
		return calc.ErrIsUnknow(arg)
	}
	var dt calc.DisplayType
	dtParsed := true
	switch arg[1] {
	case 'n', 'N':
		dt = calc.DisplayAuto
	case 'd', 'D':
		dt = calc.DisplayDecimal
	case 's', 'S':
		dt = calc.DisplayScientific
	case 'r', 'R':
		dt = calc.DisplayRational
	default:
		dtParsed = false
	}
	if !dtParsed {
		return calc.ErrIsUnknow(arg)
	}
	auto := arg[1] >= 'a' && arg[1] <= 'z'
	cf := calc.FormatConfig{
		DisplayType:   dt,
		Precision:     precision,
		PrecisionAuto: auto,
	}
	f := cf.Number
	if arg[0] == 'p' {
		c.PrintLast(f)
	} else {
		c.PrintStack(f)
	}
	return nil
}
func execprintreg(c *calc.Calc, arg string) error {
	if len(arg) < 4 {
		return calc.ErrIsUnknow(arg)
	}
	precision, err := strconv.Atoi(arg[3:])
	if err != nil {
		return calc.ErrIsUnknow(arg)
	}
	var dt calc.DisplayType
	dtParsed := true
	switch arg[2] {
	case 'n', 'N':
		dt = calc.DisplayAuto
	case 'd', 'D':
		dt = calc.DisplayDecimal
	case 's', 'S':
		dt = calc.DisplayScientific
	case 'r', 'R':
		dt = calc.DisplayRational
	default:
		dtParsed = false
	}
	if !dtParsed {
		return calc.ErrIsUnknow(arg)
	}
	auto := arg[1] >= 'a' && arg[1] <= 'z'
	cf := calc.FormatConfig{
		DisplayType:   dt,
		Precision:     precision,
		PrecisionAuto: auto,
	}
	f := cf.Number
	c.PrintRegisters(f)
	return nil
}
func execconv(c *calc.Calc, arg string) error {
	base, err := strconv.Atoi(arg[1:])
	if err != nil || base < 2 || base > 63 {
		return calc.ErrIsUnknow(arg)
	}
	c.Op1(conv(base))
	return nil
}
func execreg(c *calc.Calc, arg string) error {
	if len(arg) < 2 {
		return calc.ErrIsUnknow(arg)
	}
	var f func(string)
	switch arg[1] {
	case 'P':
		return execprintreg(c, arg)
	case 'L':
		f = c.Load
	case 's':
		f = c.StoreLast
	case 'S':
		f = c.StoreStack
	case 'c':
		f = c.ClearRegister
	}
	if f == nil {
		return calc.ErrIsUnknow(arg)
	}
	f(arg[2:])
	return nil
}
func execmac(c *calc.Calc, arg string) error {
	if len(arg) < 2 {
		return calc.ErrIsUnknow(arg)
	}
	var f func(string)
	switch arg[1] {
	case 'x':
		f = c.ExecMacro
	case 'c':
		f = c.ClearMacro
	}
	if f == nil {
		return calc.ErrIsUnknow(arg)
	}
	f(arg[2:])
	return nil
}

func execarg(c *calc.Calc, arg string) error {
	if cc, exist := opc[arg]; exist {
		cc(c)
		return nil
	}
	if o, exist := op1[arg]; exist {
		c.Op1(o(c))
		return nil
	}
	if o, exist := op2[arg]; exist {
		c.Op2(o(c))
		return nil
	}
	switch arg[0] {
	case 'o':
		return execopt(c, arg)
	case 'p', 'P':
		return execprint(c, arg)
	case 'c':
		return execconv(c, arg)
	case ':':
		return execreg(c, arg)
	case ';':
		return execmac(c, arg)
	default:
		n, base, ok := number.Parse(arg)
		if !ok {
			return calc.ErrIsUnknow(arg)
		}
		n2 := &calc.Number{
			Number: n,
		}
		n2.SetBase(base)
		c.AddStack(n2)
		return nil
	}
	return calc.ErrIsUnknow(arg)
}

func isMacro(arg string) bool { return len(arg) >= 2 && arg[0] == '[' && arg[len(arg)-1] == ']' }
func getIf(args *[]string) (e []string, ok bool) {
	if ok = len(*args) > 0; ok {
		e = append(e, (*args)[0])
		*args = (*args)[1:]
		if isMacro(e[0]) {
			if ok = len(*args) > 0; ok {
				e = append(e, (*args)[0])
				*args = (*args)[1:]
			}
		}
	}
	return
}

func execif(c *calc.Calc, args *[]string) error {
	n, err := c.Stack.Pop()
	first := err == nil && !number.IsNan(n) && !number.Is(n, 0)
	if e1, ok := getIf(args); ok {
		if e2, ok := getIf(args); ok {
			if first {
				*args = append(e1, (*args)...)
			} else {
				*args = append(e2, (*args)...)
			}
		}
		return nil
	}
	*args = []string{}
	return calc.ErrLenLt2("la liste des arguments restants à parser")
}

func exec(c *calc.Calc, args ...string) error {
	for len(args) > 0 {
		arg := args[0]
		args = args[1:]
		var err error
		if arg == "?" {
			err = execif(c, &args)
		} else if isMacro(arg) {
			var m string
			if len(args) > 0 {
				m = args[0]
				args = args[1:]
			}
			c.SetMacro(m, arg[1:len(arg)-1])
		} else {
			err = execarg(c, arg)
		}
		if err != nil {
			c.Errorln(err)
			c.Stack.ClearAll()
		}
	}
	return nil
}

var spl = func(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	begin, is_macro := -1, false
	for advance < len(data) {
		r, w := utf8.DecodeRune(data[advance:])
		if !unicode.IsSpace(r) {
			begin, is_macro = advance, r == '['
			advance += w
			break
		}
		advance += w
	}
	if begin < 0 {
		return 0, nil, nil
	}
	m := 0
	if is_macro {
		m++
	}
	end := advance
	for advance < len(data) {
		r, w := utf8.DecodeRune(data[advance:])
		advance += w
		if is_macro {
			if r == '[' {
				m++
			} else if r == ']' {
				m--
				if m == 0 {
					end = advance
					break
				}
			}
		} else {
			if unicode.IsSpace(r) {
				break
			}
		}
		end = advance
	}
	if m != 0 {
		return advance, nil, errors.New("crochet [ non terminé")
	}
	return advance, data[begin:end], nil
}

func parse(line string) (args []string, err error) {
	r := strings.NewReader(line)
	sc := bufio.NewScanner(r)
	sc.Split(spl)
	for sc.Scan() {
		args = append(args, sc.Text())
	}
	err = sc.Err()
	return
}

type App struct {
	terminal console.Terminal
	calc     *calc.Calc
}

func New(terminal console.Terminal) *App {
	app := &App{
		terminal: terminal,
		calc:     calc.New(),
	}
	app.calc.Callback = exec
	app.calc.Parser = parse
	return app
}

func (app *App) exec(arg string) {
	args, err := app.calc.Parser(arg)
	if err == nil {
		app.calc.Execute(args...)
		return
	}
	app.calc.Errorln(err)
	app.calc.Stack.ClearAll()
}

func (app *App) loop() {
	for {
		arg, err := app.terminal.Prompt("> ")
		if err != nil {
			app.calc.Errorln(err)
			break
		}
		if arg == "q" {
			break
		}
		app.terminal.AppendHistory(arg)
		app.exec(arg)
	}
}

func (app *App) Execute(args []string) {
	if len(args) <= 1 {
		app.loop()
		return
	}
	app.exec(strings.Join(args[1:], " "))
}
