package main

import (
	"os"

	"framagit.org/benjamin.vaudour/go-calc/api/cli"
	"framagit.org/benjamin.vaudour/shell/console/readline"
)

func main() {
	term := new(readline.Line)
	app := cli.New(term)
	app.Execute(os.Args)
}
