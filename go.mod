module framagit.org/benjamin.vaudour/go-calc

go 1.15

require (
	framagit.org/benjamin.vaudour/number v1.1.0
	framagit.org/benjamin.vaudour/shell v1.1.0
	framagit.org/benjamin.vaudour/strutil v1.0.1
)
